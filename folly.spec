Name:           folly
Version:        2019.07.29.00
Release:        1%{?dist}
Summary:        Facebook Open-source Library
License:        ASL 2.0
URL:            https://github.com/facebook/folly
Source0:        https://github.com/facebook/folly/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  binutils-devel
BuildRequires:  openssl-devel
BuildRequires:  boost-devel
BuildRequires:  libevent-devel
BuildRequires:  lz4-devel
BuildRequires:  xz-devel
BuildRequires:  snappy-devel
BuildRequires:  zlib-devel
BuildRequires:  double-conversion-devel
BuildRequires:  glog-devel
BuildRequires:  gflags-devel
BuildRequires:  libaio-devel
BuildRequires:  libaio-devel
BuildRequires:  libdwarf-devel
BuildRequires:  libunwind-devel
BuildRequires:  libsodium-devel
# BuildRequires:  gtest-devel
# BuildRequires:  gmock-devel

%description
Folly (acronymed loosely after Facebook Open Source Library) is a library of
C++14 components designed with practicality and efficiency in mind. Folly
contains a variety of core library components used extensively at Facebook. In
particular, it's often a dependency of Facebook's other open source C++ efforts
and place where those projects can share code.

It complements (as opposed to competing against) offerings such as Boost and of
course std. In fact, we embark on defining our own component only when
something we need is either not available, or does not meet the needed
performance profile. We endeavor to remove things from folly if or when std or
Boost obsoletes them.

Performance concerns permeate much of Folly, sometimes leading to designs that
are more idiosyncratic than they would otherwise be (see e.g. PackedSyncPtr.h,
SmallLocks.h). Good performance at large scale is a unifying theme in all of
Folly.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       cmake-filesystem
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup

%build
%cmake . \
    -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
    -DCMAKE_INSTALL_DIR=share/cmake/Modules \
    -DBUILD_SHARED_LIBS=ON \
    -DBoost_USE_STATIC_LIBS=OFF \
    -DUSE_CMAKE_GOOGLE_TEST_INTEGRATION=OFF \
    -DBUILD_TESTS=OFF

%make_build

%install
%make_install

%check
ctest -V %{?_smp_mflags}

%ldconfig_scriptlets

%files
%license LICENSE
%doc README.md

%files devel
%{_includedir}/folly/*
%{_libdir}/libfolly.so
%{_libdir}/libfollybenchmark.so
%{_libdir}/libfolly_test_util.so
%{_libdir}/libfolly_exception_counter.so
%{_libdir}/libfolly_exception_tracer.so
%{_libdir}/libfolly_exception_tracer_base.so
%{_libdir}/pkgconfig/libfolly.pc
%{_datadir}/cmake/Modules/*.cmake

%changelog
* Mon Dec 03 2018 Sergey Avseyev <sergey.avseyev@gmail.com> 1.0.0-1
- initial package

